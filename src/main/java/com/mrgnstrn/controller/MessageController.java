package com.mrgnstrn.controller;


import com.mrgnstrn.entity.Activity;
import com.mrgnstrn.service.ActivityService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class MessageController {

    @Autowired
    private ActivityService activityService;

    @RequestMapping(value = "/api/messages", method = RequestMethod.POST)
    public void getMessage(@RequestBody Activity activity) {
        long timestart = System.currentTimeMillis();
        activityService.consume(activity);

        System.out.println(System.currentTimeMillis() - timestart);
    }


}
