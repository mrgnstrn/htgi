package com.mrgnstrn.builder;

import com.mrgnstrn.entity.Activity;
import org.springframework.stereotype.Service;

@Service
public class SimpleActivityBuilder {

    public Activity createSimpleActivity(Activity activity, String message) {
        Activity reply = new Activity();
        reply.setType(activity.getType());
        reply.setConversation(activity.getConversation());
        reply.setFrom(activity.getRecipient());
        reply.setServiceUrl(activity.getServiceUrl());
        reply.setLocale(activity.getLocale());
        reply.setType(activity.getType());
        reply.setText(message);
        return reply;
    }


}
