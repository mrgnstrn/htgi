package com.mrgnstrn.entity;

public class Activity {

    private String id;
    private String type;
    private String text;
    private ChannelAccount from;
    private ChannelAccount recipient;
    private ConversationAccount conversation;
    private String serviceUrl;
    private String timestamp;
    private String locale;

    public String getLocale() {
        return locale;
    }

    public void setLocale(String locale) {
        this.locale = locale;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public ChannelAccount getFrom() {
        return from;
    }

    public void setFrom(ChannelAccount from) {
        this.from = from;
    }

    public ChannelAccount getRecipient() {
        return recipient;
    }

    public void setRecipient(ChannelAccount recipient) {
        this.recipient = recipient;
    }

    public ConversationAccount getConversation() {
        return conversation;
    }

    public void setConversation(ConversationAccount conversation) {
        this.conversation = conversation;
    }

    public String getServiceUrl() {
        return serviceUrl;
    }

    public void setServiceUrl(String serviceUrl) {
        this.serviceUrl = serviceUrl;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    @Override
    public String toString() {
        return "Activity{" +
                "type='" + type + '\'' +
                ", text='" + text + '\'' +
                ", from=" + from +
                ", recipient=" + recipient +
                ", conversation=" + conversation +
                ", serviceUrl='" + serviceUrl + '\'' +
                ", timestamp='" + timestamp + '\'' +
                '}';
    }
}
