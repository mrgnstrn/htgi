package com.mrgnstrn.entity;

public class ConversationAccount {
    private String id;
    private boolean isGroup;
    private String name;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public boolean isGroup() {
        return isGroup;
    }

    public void setGroup(boolean group) {
        isGroup = group;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "ConversationAccount{" +
                "id='" + id + '\'' +
                ", isGroup=" + isGroup +
                ", name='" + name + '\'' +
                '}';
    }
}
