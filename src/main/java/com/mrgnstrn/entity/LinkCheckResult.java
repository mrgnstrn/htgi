package com.mrgnstrn.entity;

import java.time.LocalDateTime;

public class LinkCheckResult {

    private String link;
    private String firstTimePoster;
    private int occurencies;
    private LocalDateTime firstTimePosted;

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getFirstTimePoster() {
        return firstTimePoster;
    }

    public void setFirstTimePoster(String firstTimePoster) {
        this.firstTimePoster = firstTimePoster;
    }

    public int getOccurencies() {
        return occurencies;
    }

    public void setOccurencies(int occurencies) {
        this.occurencies = occurencies;
    }

    public LocalDateTime getFirstTimePosted() {
        return firstTimePosted;
    }

    public void setFirstTimePosted(LocalDateTime firstTimePosted) {
        this.firstTimePosted = firstTimePosted;
    }
}
