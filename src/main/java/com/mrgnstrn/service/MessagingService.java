package com.mrgnstrn.service;

import com.mrgnstrn.entity.Activity;

public interface MessagingService {

    void sendMessage(Activity activity);
}
