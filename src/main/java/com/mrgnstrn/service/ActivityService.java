package com.mrgnstrn.service;

import com.mrgnstrn.entity.Activity;

public interface ActivityService {

    void consume(Activity activity);

}
