package com.mrgnstrn.service;

import java.time.LocalDate;

public interface UptimeService {

    LocalDate getStartupTime();

    String getUptime();
}
