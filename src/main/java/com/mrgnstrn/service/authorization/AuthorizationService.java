package com.mrgnstrn.service.authorization;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mrgnstrn.service.impl.HttpServerService;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

@Service
public class AuthorizationService {

    @Value("${microsoft.app.id}")
    private String MICROSOFT_APP_ID;
    @Value("${microsoft.app.password}")
    private String MICROSOFT_APP_PASSWORD;

    @Autowired
    private HttpServerService httpServerService;

    private ObjectMapper mapper;

    @PostConstruct
    private void init() {
        mapper = new ObjectMapper();
    }

    public AuthorizationToken authorize() {

        URI uri = URI.create("https://login.microsoftonline.com/botframework.com/oauth2/v2.0/token");
        Map<String, String> headers = new HashMap<>();

        headers.put("Host", "login.microsoftonline.com");
        headers.put("Content-Type", "application/x-www-form-urlencoded");

        String body = "grant_type=client_credentials&client_id=MICROSOFT-APP-ID&client_secret=MICROSOFT-APP-PASSWORD&scope=https%3A%2F%2Fapi.botframework.com%2F.default";
        body = body.replaceAll("MICROSOFT-APP-ID", MICROSOFT_APP_ID);
        body = body.replaceAll("MICROSOFT-APP-PASSWORD", MICROSOFT_APP_PASSWORD);

        AuthorizationToken authorizationToken = null;
        try (CloseableHttpResponse response = httpServerService.sendPost(uri, body, headers)) {
            HttpEntity entity = response.getEntity();
            InputStream content = entity.getContent();
            String result = IOUtils.toString(content, String.valueOf(StandardCharsets.UTF_8));
            authorizationToken = mapper.readValue(result, AuthorizationToken.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return authorizationToken;

    }
}
