package com.mrgnstrn.service.impl;


import com.mrgnstrn.service.MessageParseService;
import org.nibor.autolink.LinkExtractor;
import org.nibor.autolink.LinkSpan;
import org.nibor.autolink.LinkType;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.*;


@Service
public class MessageParseServiceImpl implements MessageParseService {

    private LinkExtractor linkExtractor;

    @PostConstruct
    private void init() {
        linkExtractor = LinkExtractor.builder().linkTypes(EnumSet.of(LinkType.URL, LinkType.WWW)).build();
    }

    @Override
    public Optional<String> getFirstUrl(String text) {
        Iterable<LinkSpan> links = linkExtractor.extractLinks(text);
        Optional<String> result = Optional.empty();

        if (links.iterator().hasNext()) {
            LinkSpan next = links.iterator().next();
            result = Optional.of(text.substring(next.getBeginIndex(), next.getEndIndex()));
        }
        return result;
    }

    @Override
    public List<String> getAllUrls(String text) {
        Iterable<LinkSpan> links = linkExtractor.extractLinks(text);
        List<String> result = new ArrayList<>();

        for (LinkSpan next : links) {
            result.add(text.substring(next.getBeginIndex(), next.getEndIndex()));
        }
        return result;
    }
}
