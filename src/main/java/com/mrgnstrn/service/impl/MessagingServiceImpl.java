package com.mrgnstrn.service.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mrgnstrn.entity.Activity;
import com.mrgnstrn.service.MessagingService;
import com.mrgnstrn.service.authorization.AuthorizationService;
import com.mrgnstrn.service.authorization.AuthorizationToken;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;


import javax.annotation.PostConstruct;
import java.io.IOException;
import java.net.URI;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

@Service
public class MessagingServiceImpl implements MessagingService {

    @Value("${endpoint}")
    private String serviceEndpoint;

    @Autowired
    private HttpServerService httpServerService;
    @Autowired
    private AuthorizationService authorizationService;

    private ObjectMapper mapper;
    private AuthorizationToken token;


    @PostConstruct
    private void init() {
        mapper = new ObjectMapper();
        token = authorizationService.authorize();
    }


    @Override
    public void sendMessage(Activity activity) {

        String endpoint = serviceEndpoint.replace("{conversationId}", activity.getConversation().getId());
        URI uri = URI.create(activity.getServiceUrl() + endpoint);
        Map<String, String> headers = new HashMap<>();
        headers.put("Authorization", "Bearer " + token.getAccess_token());
        CloseableHttpResponse response = null;
        try {
            String json = mapper.writeValueAsString(activity);
            response = httpServerService.sendJsonPost(uri, json, headers);
            if (response.getStatusLine().getStatusCode() == 403) {
                token = authorizationService.authorize();
                headers.put("Authorization", "Bearer " + token.getAccess_token());
                response = httpServerService.sendJsonPost(uri, json, headers);
            }
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            close(response);
        }
    }

    private void close(CloseableHttpResponse response) {
        if (Objects.nonNull(response)) {
            try {
                response.close();
            } catch (IOException e) {
            }
        }
    }


}
