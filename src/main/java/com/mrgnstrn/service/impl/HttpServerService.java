package com.mrgnstrn.service.impl;

import org.apache.commons.io.IOUtils;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.net.URI;
import java.nio.charset.StandardCharsets;
import java.util.Map;

@Service
public class HttpServerService {

    private CloseableHttpClient httpClient;

    @PostConstruct
    private void init() {
        httpClient = HttpClients.createDefault();
    }

    public CloseableHttpResponse sendJsonPost(URI uri, String body, Map<String, String> header) throws IOException {

        CloseableHttpResponse response = null;

        HttpPost post = new HttpPost();
        post.setURI(uri);
        StringEntity entity = new StringEntity(body, ContentType.create("application/json", "UTF-8"));
        post.setEntity(entity);
        header.forEach(post::setHeader);

        return httpClient.execute(post);

    }

    public CloseableHttpResponse sendPost(URI uri, String body, Map<String, String> header) throws IOException {

        CloseableHttpResponse response = null;

        HttpPost post = new HttpPost();
        post.setURI(uri);
        StringEntity entity = new StringEntity(body, ContentType.create("application/x-www-form-urlencoded", "UTF-8"));
        post.setEntity(entity);
        header.forEach(post::setHeader);

        return httpClient.execute(post);

    }

}
