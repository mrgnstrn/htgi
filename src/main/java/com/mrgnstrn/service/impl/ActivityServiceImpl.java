package com.mrgnstrn.service.impl;

import com.mrgnstrn.builder.SimpleActivityBuilder;
import com.mrgnstrn.entity.Activity;
import com.mrgnstrn.entity.LinkCheckResult;
import com.mrgnstrn.service.ActivityService;
import com.mrgnstrn.service.LinkCounterService;
import com.mrgnstrn.service.MessageParseService;
import com.mrgnstrn.service.MessagingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class ActivityServiceImpl implements ActivityService {

    @Autowired
    private MessageParseService messageParseService;
    @Autowired
    private LinkCounterService linkCounterService;
    @Autowired
    private MessagingService messagingService;
    @Autowired
    private SimpleActivityBuilder simpleActivityBuilder;

    @Override
    public void consume(Activity activity) {

        if (activity.getType().equalsIgnoreCase("message")) {

            List<String> allUrls = messageParseService.getAllUrls(activity.getText());

            if (!allUrls.isEmpty()) {

                List<LinkCheckResult> repetiveLinks = new ArrayList<>();
                String userId = activity.getFrom().getId();
                for (String url : allUrls) {
                    Optional<LinkCheckResult> linkCheckResult = linkCounterService.store(userId, url);
                    linkCheckResult.ifPresent(repetiveLinks::add);
                }
                if (!repetiveLinks.isEmpty()) {
                    String message = generateMessage(activity.getFrom().getId(), repetiveLinks);
                    Activity simpleReply = simpleActivityBuilder.createSimpleActivity(activity, message);
                    messagingService.sendMessage(simpleReply);
                }

            }

        }

    }

    private String generateMessage(String userId, List<LinkCheckResult> repetiveLinks) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(userId)
                .append(", еб твою мать, в твоем сообщении ")
                .append(repetiveLinks.size()).append(" повторящаяся ссылка: ")
                .append(System.lineSeparator());
        repetiveLinks.forEach(linkCheckResult -> stringBuilder.append(linkCheckResult.getLink())
                .append(" была запощена ")
                .append(linkCheckResult.getFirstTimePoster())
                .append(" первый раз ")
                .append(formatTime(linkCheckResult.getFirstTimePosted()))
                .append(System.lineSeparator()));
        return stringBuilder.toString();
    }

    private String formatTime(LocalDateTime time) {

        return String.valueOf(time.getDayOfMonth()) +
                "." +
                time.getMonth().getValue() +
                "." +
                time.getYear() +
                " в " +
                time.getHour() +
                ":" +
                time.getMinute();
    }
}
