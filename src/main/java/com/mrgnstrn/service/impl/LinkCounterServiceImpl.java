package com.mrgnstrn.service.impl;

import com.mrgnstrn.entity.LinkCheckResult;
import com.mrgnstrn.service.LinkCounterService;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.time.LocalDateTime;
import java.util.*;

@Service
public class LinkCounterServiceImpl implements LinkCounterService {

    Map<String, LinkStoreEntry> storage;

    @PostConstruct
    private void init() {
        storage = new HashMap<>();
    }

    @Override
    public Optional<LinkCheckResult> store(String userId, String link) {

        LinkStoreEntry linkStoreEntry = createLinkStoreEntry(link, userId);

        Optional<LinkCheckResult> checkResult = check(link);
        storage.put(link, linkStoreEntry);

        return checkResult;


    }

    private LinkStoreEntry createLinkStoreEntry(String link, String userId) {

        LinkStoreEntry linkStoreEntry = storage.get(link);
        if (Objects.nonNull(linkStoreEntry)) {
            linkStoreEntry.setPostOccurencies(linkStoreEntry.getPostOccurencies() + 1);
        } else {
            linkStoreEntry = new LinkStoreEntry();
            linkStoreEntry.setFirstTimePoster(userId);
            linkStoreEntry.setFirstTimePosted(LocalDateTime.now());
            linkStoreEntry.setPostOccurencies(1);
        }
        return linkStoreEntry;
    }

    @Override
    public Optional<LinkCheckResult> check(String link) {

        Optional<LinkCheckResult> result = Optional.empty();
        LinkStoreEntry linkStoreEntry = storage.get(link);
        if (Objects.nonNull(linkStoreEntry)) {
            result = Optional.of(createLinkCheckResult(link, linkStoreEntry));
        }
        return result;

    }

    private LinkCheckResult createLinkCheckResult(String link, LinkStoreEntry linkStoreEntry) {
        LinkCheckResult linkCheckResult = new LinkCheckResult();

        linkCheckResult.setFirstTimePosted(linkStoreEntry.getFirstTimePosted());
        linkCheckResult.setFirstTimePoster(linkStoreEntry.firstTimePoster);
        linkCheckResult.setLink(link);
        linkCheckResult.setOccurencies(linkStoreEntry.getPostOccurencies());

        return linkCheckResult;

    }


    private class LinkStoreEntry {
        private String firstTimePoster;
        private int postOccurencies;
        private LocalDateTime firstTimePosted;

        public String getFirstTimePoster() {
            return firstTimePoster;
        }

        public void setFirstTimePoster(String firstTimePoster) {
            this.firstTimePoster = firstTimePoster;
        }

        public int getPostOccurencies() {
            return postOccurencies;
        }

        public void setPostOccurencies(int postOccurencies) {
            this.postOccurencies = postOccurencies;
        }

        public LocalDateTime getFirstTimePosted() {
            return firstTimePosted;
        }

        public void setFirstTimePosted(LocalDateTime firstTimePosted) {
            this.firstTimePosted = firstTimePosted;
        }
    }

}
