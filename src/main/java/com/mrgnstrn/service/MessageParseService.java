package com.mrgnstrn.service;

import java.util.List;
import java.util.Optional;

public interface MessageParseService {

    Optional<String> getFirstUrl(String text);

    List<String> getAllUrls(String text);

}
