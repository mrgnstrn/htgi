package com.mrgnstrn.service;

import com.mrgnstrn.entity.LinkCheckResult;

import java.util.Optional;

public interface LinkCounterService {

    Optional<LinkCheckResult> store(String userId, String link);

    Optional<LinkCheckResult> check(String link);

}
