package com.mrgnstrn;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HtgiApplication {

	public static void main(String[] args) {
		SpringApplication.run(HtgiApplication.class, args);
	}
}
